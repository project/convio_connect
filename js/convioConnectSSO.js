(function ($, Drupal, settings) {
  var sso = new Drupal.convioConnect(settings.convioConnect);

  sso.success = function (data){
    console.log(data);
    console.log('I am the new success function');
  };

  Drupal.behaviors.convioConnectSSO = {
    attach: function (context, settings) {
      sso.request();
    }
  };

})(jQuery, Drupal, drupalSettings);