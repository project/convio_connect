(function($, Drupal){
  var convioConnect = Drupal.convioConnect = function(options) {
    this.options = $.extend({
      type: 'GET',
      url: '',
      dataType: 'json',
      cache: false,
      crossDomain: true,
      xhrFields: { withCredentials: true }
    }, options)
  };

  convioConnect.prototype.request = function() {
    var self = this;

    $.ajax(this.options)
      .done(function(data){
        self.success(data);
      });
  };

  convioConnect.prototype.success = function(data) {};

  convioConnect.prototype.success = function(data) {};

})(jQuery, Drupal);