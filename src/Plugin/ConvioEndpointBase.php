<?php

namespace Drupal\convio_connect\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\convio_connect\Convio\ConvioConnectApi;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Database\Connection;
use DateTime;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Convio Endpoint plugins.
 */
abstract class ConvioEndpointBase extends PluginBase implements ConvioEndpointInterface, ContainerFactoryPluginInterface {

  /**
   * The Convio Connect Api.
   *
   * @var \Drupal\convio_connect\Convio\ConvioConnectApi
   */
  protected $convioConnectApi;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The api servlet.
   *
   * @var string
   */
  protected $servlet;

  /**
   * The api method.
   *
   * @var string
   */
  protected $method;

  /**
   * The api method.
   *
   * @var string
   */
  protected $isFrozen;

  /**
   * Determines whether or not an auth token should be added to request.
   *
   * @var bool
   */
  protected $authRequired;

  /**
   * Determines whether or not the api response should be stored.
   *
   * @var bool
   */
  protected $cacheLifetime;

  /**
   * Additional parameters that are passed to the API.
   *
   * @var array
   */
  protected $params;

  /**
   * The custom api url.
   *
   * @var string
   */
  protected $customUrl;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $sandwich = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('convio_connect.convio_connect_api')
    );
    return $sandwich;
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, ConvioConnectApi $convioConnectApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->convioConnectApi = $convioConnectApi;
    $this->connection = $connection;
    $this->servlet = $this->getServlet();
    $this->method = $this->getMethod();
    $this->cacheLifetime = $this->cacheLifetime();
    $this->params = $this->getParams();
    $this->authRequired = $this->authRequired();
    $this->isFrozen = $this->isFrozen();
    $this->customUrl = $this->getCustomUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getServlet() {
    return isset($this->pluginDefinition['servlet']) ? $this->pluginDefinition['servlet'] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMethod() {
    return $this->pluginDefinition['method'];
  }

  /**
   * {@inheritdoc}
   */
  public function getParams() {
    $config = $this->configuration;
    if (isset($config['isFrozen'])) {
      unset($config['isFrozen']);
    }
    return array_merge($this->pluginDefinition['params'], $config);
  }

  /**
   * {@inheritdoc}
   */
  public function authRequired() {
    return $this->pluginDefinition['authRequired'];
  }

  /**
   * {@inheritdoc}
   */
  public function isFrozen() {
    return isset($this->configuration['isFrozen']) ? $this->configuration['isFrozen'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function cacheLifetime() {
    return $this->pluginDefinition['cacheLifetime'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomUrl() {
    return isset($this->pluginDefinition['customUrl']) ? $this->pluginDefinition['customUrl'] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadData() {
    // If plugin cacheLifetime() > 0 check the db for a stored response.
    // TODO: Look into using the expired field to fetch a fresh copy.
    // TODO: Add timeouts.
    if ($this->cacheLifetime() > 0 || $this->isFrozen) {
      $query = $this->connection->select('convio_connect_api_data', 'cc');
      $query->fields('cc', ['data']);
      $query->condition('cc.request_id', $this->generateRequestId());
      if ($result = $query->execute()->fetchField()) {
        return unserialize($result);
      }
    }

    return $this->requestData();
  }

  /**
   * Function used to grab data from the Convio Connect Api and store it.
   *
   * Note the response is stored in the database instead of cache to allow
   * administrators the ability to "freeze" data or to take a snapshot in time.
   *
   * @return bool|mixed
   *   Api response data.
   */
  public function requestData() {
    if ($data = $this->convioConnectApi->request($this->servlet, $this->method, $this->params, 'get', $this->authRequired, $this->customUrl)) {
      // If plugin cacheLifetime() > 0 then store response in database.
      if ($this->cacheLifetime > 0) {
        $this->storeData($data);
      }
      return $data;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Function used to post data to the Convio Connect Api.
   *
   * @return bool|mixed
   *   Api response data.
   */
  public function postData() {
    if ($data = $this->convioConnectApi->request($this->servlet, $this->method, $this->params, 'post', $this->authRequired, $this->customUrl)) {
      return $data;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper function used to save or update response data.
   *
   * @param object $data
   *   Convio response object.
   */
  public function storeData($data) {
    $date = new DateTime();

    $this->connection->merge('convio_connect_api_data')
      ->key(['request_id' => $this->generateRequestId()])
      ->fields([
        'plugin_id' => $this->getPluginId(),
        'params' => serialize($this->configuration),
        'data' => serialize($data),
        'frozen' => $this->isFrozen,
        'created' => $date->getTimestamp(),
        'expired' => $this->cacheLifetime > 0 ? $date->getTimestamp() + $this->cacheLifetime : $this->cacheLifetime,
      ])
      ->execute();
  }

  /**
   * Helper function used to build unique request_id.
   *
   * @return string
   *   Unique request_id.
   */
  public function generateRequestId() {
    return md5(serialize([$this->getPluginId(), $this->params]));
  }

  /**
   * Helper function used to get plugin settings for use on the client side.
   *
   * @return array|bool
   *   Array of settings for clientside calls.
   */
  public function getClientSideSettings() {
    if ($base_settings = $this->convioConnectApi->getSettings()) {
      if ($this->customUrl) {
        $url = $this->customUrl;
      }
      else {
        $url_obj = Url::fromUri('https://' . $base_settings['host'] . '/' . $base_settings['short_name'] . '/site/' . $this->servlet);
        $url = $url_obj->toString();
      }
      $base_settings['settings']['method'] = $this->method;

      return [
        'url' => $url,
        'data' => array_merge($base_settings['settings'], $this->params),
      ];
    }

    return FALSE;
  }

}
