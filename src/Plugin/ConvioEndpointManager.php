<?php

namespace Drupal\convio_connect\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Convio Endpoint plugin manager.
 */
class ConvioEndpointManager extends DefaultPluginManager {

  /**
   * Constructs a new ConvioEndpointManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ConvioEndpoint', $namespaces, $module_handler, 'Drupal\convio_connect\Plugin\ConvioEndpointInterface', 'Drupal\convio_connect\Annotation\ConvioEndpoint');

    $this->alterInfo('convio_connect_convio_endpoint_info');
    $this->setCacheBackend($cache_backend, 'convio_connect_convio_endpoint_plugins');
  }

}
