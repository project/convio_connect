<?php

namespace Drupal\convio_connect\Plugin\ConvioEndpoint;

use Drupal\convio_connect\Plugin\ConvioEndpointBase;
use Drupal\convio_connect\Plugin\ConvioEndpointInterface;

/**
 * Calls the getCompanies method.
 *
 * @ConvioEndpoint(
 *  id = "getCompanies",
 *  label = @Translation("The plugin ID."),
 *  servlet = "CRTeamraiserAPI",
 *  method = "getCompaniesByInfo",
 *  authRequired = FALSE,
 *  cacheLifetime = 3600,
 *  params = {
 *    "event_type3" = "CE_Live"
 *  }
 * )
 */
class Companies extends ConvioEndpointBase implements ConvioEndpointInterface {

}
