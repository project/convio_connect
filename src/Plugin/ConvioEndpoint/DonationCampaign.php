<?php

namespace Drupal\convio_connect\Plugin\ConvioEndpoint;

use Drupal\convio_connect\Plugin\ConvioEndpointBase;
use Drupal\convio_connect\Plugin\ConvioEndpointInterface;

/**
 * Calls the getCompanies method.
 *
 * @ConvioEndpoint(
 *  id = "donationCampaign",
 *  label = @Translation("The plugin ID."),
 *  customUrl = "https://secure2.convio.net/mskcc/site/SPageServer",
 *  method = "getDonationCampaignInfo",
 *  authRequired = FALSE,
 *  cacheLifetime = 3600,
 *  params = {
 *    "pagename" = "campaign_prog_API",
 *    "campaign_id" = "4564",
 *    "pgwrap" = "n",
 *    "callback" = "parseResponse"
 *  }
 * )
 */
class DonationCampaign extends ConvioEndpointBase implements ConvioEndpointInterface {}
