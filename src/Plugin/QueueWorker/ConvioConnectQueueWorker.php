<?php

namespace Drupal\convio_connect\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\convio_connect\Plugin\ConvioEndpointManager;

/**
 * Processes expired convio_connect data.
 *
 * @QueueWorker(
 *  id = "convio_connect_queue",
 *  title = @Translation("Convio Connect Queue"),
 *  cron = {"time" = 60}
 * )
 */
class ConvioConnectQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The ConvioEndpoint Plugin Manager.
   *
   * @var \Drupal\convio_connect\Plugin\ConvioEndpointManager
   */
  protected $convioEndpointManager;

  /**
   * The Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        ConvioEndpointManager $convioEndpointManager,
        LoggerChannelFactoryInterface $loggerFactory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->convioEndpointManager = $convioEndpointManager;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.convio_endpoint'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    if ($endpoint = $this->convioEndpointManager->createInstance($item['plugin_id'], $item['params'])) {
      $endpoint->requestData();
      $this->loggerFactory->get('convio_connect')
        ->info('@plugin was updated', ['@plugin' => $item['plugin_id']]);
    }
  }

}
