<?php

namespace Drupal\convio_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\convio_connect\Convio\ConvioConnectApi;
use Drupal\convio_connect\Plugin\ConvioEndpointManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DemoController.
 *
 * @package Drupal\convio_connect\Controller
 */
class DemoController extends ControllerBase {

  /**
   * The Convio Connect Api.
   *
   * @var \Drupal\convio_connect\Convio\ConvioConnectApi
   */
  protected $convioConnectApi;

  /**
   * The ConvioEndpoint Plugin Manager.
   *
   * @var \Drupal\convio_connect\Plugin\ConvioEndpointManager
   */
  protected $convioEndpointManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('convio_connect.convio_connect_api'),
      $container->get('plugin.manager.convio_endpoint')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConvioConnectApi $convioConnectApi, ConvioEndpointManager $convioEndpointManager) {
    $this->convioConnectApi = $convioConnectApi;
    $this->convioEndpointManager = $convioEndpointManager;
  }

  /**
   * Hello.
   */
  public function demo() {
    $params = [
      'isFrozen' => TRUE,
      'company_name' => '%dan%',
    ];

    /** @var \Drupal\convio_connect\Plugin\ConvioEndpoint\Companies $companies */
    $companies = $this->convioEndpointManager->createInstance('getCompanies', $params);
    kint($companies->loadData());

    /** @var \Drupal\convio_connect\Plugin\ConvioEndpoint\DonationCampaign $campaign */
    $campaign = $this->convioEndpointManager->createInstance('donationCampaign');
    $c_settings = $campaign->getClientSideSettings();

    /*$listSurveys = $this->convioEndpointManager->createInstance('listSurveys');
    $surveys = $listSurveys->loadData();
    $options = [];
    if (isset($surveys->listSurveysResponse) && isset($surveys->listSurveysResponse->surveys)) {
      foreach ($surveys->listSurveysResponse->surveys as $survey) {
        $options[$survey->surveyId] = $survey->surveyName;
      }
    }
    ksm($options);*/

    $survey = $listSurveys = $this->convioEndpointManager->createInstance('getSurvey', ['survey_id' => 5862]);
    $fields = $survey->loadData();

    kint($fields);

    //kint($c_settings);

    /*$params = [
    'survey_id' => '5862',
    'question_17101' => $this->t('Yay it should be working again'),
    ];
    $survey_result = $companies = $this->convioEndpointManager->createInstance('submitSurvey', $params);
    kint($survey_result->postData());*/


    return [
      '#type' => 'markup',
      '#markup' => $this->t('Hello there!'),
      '#attached' => [
        'library' => ['convio_connect/convioClient'],
        'drupalSettings' => [
          'convioConnect' => $c_settings,
        ],
      ],
    ];
  }

}
