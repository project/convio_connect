<?php

namespace Drupal\convio_connect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Convio Connect Settings Form.
 *
 * This form is used to get settings for the Convio Connect Api.
 *
 * @package Drupal\convio_connect\Form
 */
class ConvioConnectForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['convio_connect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'convio_connect_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('convio_connect.settings');
    $form['convio_connect_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api Key'),
      '#default_value' => $config->get('api_key'),
      '#required' => TRUE,
    ];
    $form['convio_connect_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $config->get('host'),
      '#required' => TRUE,
    ];
    $form['convio_connect_short_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short Name'),
      '#default_value' => $config->get('short_name'),
      '#required' => TRUE,
    ];
    $form['convio_connect_login_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login Name'),
      '#default_value' => $config->get('login_name'),
      '#required' => TRUE,
    ];
    $form['convio_connect_login_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login Password'),
      '#default_value' => $config->get('login_password'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('convio_connect.settings');
    $config
      ->set('api_key', $form_state->getValue('convio_connect_api_key'))
      ->set('host', $form_state->getValue('convio_connect_host'))
      ->set('short_name', $form_state->getValue('convio_connect_short_name'))
      ->set('login_name', $form_state->getValue('convio_connect_login_name'))
      ->set('login_password', $form_state->getValue('convio_connect_login_password'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
