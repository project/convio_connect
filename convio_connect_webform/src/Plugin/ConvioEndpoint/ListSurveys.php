<?php

namespace Drupal\convio_connect_webform\Plugin\ConvioEndpoint;

use Drupal\convio_connect\Plugin\ConvioEndpointBase;
use Drupal\convio_connect\Plugin\ConvioEndpointInterface;

/**
 * Calls the getCompanies method.
 *
 * @ConvioEndpoint(
 *  id = "listSurveys",
 *  label = @Translation("List Survey Api."),
 *  servlet = "CRSurveyAPI",
 *  method = "listSurveys",
 *  authRequired = FALSE,
 *  cacheLifetime = 0,
 *  params = {}
 * )
 */
class ListSurveys extends ConvioEndpointBase implements ConvioEndpointInterface {

}