<?php

namespace Drupal\convio_connect_webform\Plugin\ConvioEndpoint;

use Drupal\convio_connect\Plugin\ConvioEndpointBase;
use Drupal\convio_connect\Plugin\ConvioEndpointInterface;

/**
 * Calls the getCompanies method.
 *
 * @ConvioEndpoint(
 *  id = "submitSurvey",
 *  label = @Translation("Submit Survey Api."),
 *  servlet = "CRSurveyAPI",
 *  method = "submitSurvey",
 *  authRequired = TRUE,
 *  cacheLifetime = 0,
 *  params = {}
 * )
 */
class SubmitSurvey extends ConvioEndpointBase implements ConvioEndpointInterface {

}
