<?php

namespace Drupal\convio_connect_webform\Plugin\ConvioEndpoint;

use Drupal\convio_connect\Plugin\ConvioEndpointBase;
use Drupal\convio_connect\Plugin\ConvioEndpointInterface;

/**
 * Calls the getCompanies method.
 *
 * @ConvioEndpoint(
 *  id = "getSurvey",
 *  label = @Translation("List Survey Api."),
 *  servlet = "CRSurveyAPI",
 *  method = "getSurvey",
 *  authRequired = TRUE,
 *  cacheLifetime = 0,
 *  params = {}
 * )
 */
class GetSurvey extends ConvioEndpointBase implements ConvioEndpointInterface {

}