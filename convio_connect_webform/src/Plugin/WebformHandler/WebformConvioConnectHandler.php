<?php

namespace Drupal\convio_connect_webform\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\convio_connect\Plugin\ConvioEndpointManager;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Component\Utility\Html;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Submits webform submissions to Convio Surveys.
 *
 * @WebformHandler(
 *  id = "convio_connect",
 *  label = @Translation("Convio Connect."),
 *  category = @Translation("Convio Connect."),
 *  description = @Translation("Sends a form submission to a Convio Survey."),
 *  cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *  results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformConvioConnectHandler extends WebformHandlerBase {

  /**
   * Drupal\convio_connect\Plugin\ConvioEndpointManager definition.
   *
   * @var \Drupal\convio_connect\Plugin\ConvioEndpointManager
   */
  protected $convioEndpointManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, ConvioEndpointManager $plugin_manager_convio_endpoint) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $config_factory, $entity_type_manager);
    $this->convioEndpointManager = $plugin_manager_convio_endpoint;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('convio_connect.webform'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.convio_endpoint')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#theme' => 'markup',
      '#markup' => $this->t('Conivo Survey: @survey_id', ['@survey_id' => $this->configuration['survey_id']]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'survey_id' => '',
      'convio_mapping' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form_state->disableCache();
    $form_state->setCached(FALSE);

    // Build list of surveys from the api.
    /** @var \Drupal\convio_connect_webform\Plugin\ConvioEndpoint\ListSurveys $listSurveys */
    $listSurveys = $this->convioEndpointManager->createInstance('listSurveys');
    $surveys = $listSurveys->loadData();
    $survey_options = ['' => $this->t('- Select -')];
    if (isset($surveys->listSurveysResponse) && isset($surveys->listSurveysResponse->surveys)) {
      foreach ($surveys->listSurveysResponse->surveys as $survey) {
        $survey_options[$survey->surveyId] = $survey->surveyName;
      }
    }
    else {
      drupal_set_message($this->t('There are no fields attached to this survey'), 'warning');
    }

    $form['survey_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Survey Id'),
      '#required' => TRUE,
      '#options' => $survey_options,
      '#default_value' => isset($this->configuration['survey_id']) ? $this->configuration['survey_id'] : '',
      '#ajax' => [
        'callback' => [$this, 'convioMappingCallback'],
        'wrapper' => 'convio-mapping',
      ],
    ];

    $form['convio_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Mapping'),
      '#attributes' => ['id' => 'convio-mapping'],
      '#tree' => TRUE,
    ];

    $survey_value = $form_state->getValue('survey_id');
    $survey_id = isset($survey_value) ? $survey_value : $this->configuration['survey_id'];
    if (!empty($survey_id)) {

      /** @var \Drupal\convio_connect_webform\Plugin\ConvioEndpoint\GetSurvey $survey */
      $survey = $this->convioEndpointManager->createInstance('getSurvey', ['survey_id' => $survey_id]);
      $fields = $survey->loadData();

      if (isset($fields->getSurveyResponse) && isset($fields->getSurveyResponse->survey) && isset($fields->getSurveyResponse->survey->surveyQuestions)) {
        $survey_fields = $fields->getSurveyResponse->survey->surveyQuestions;

        // API returns a single object if just there is just one field for the
        // selected survey.
        if (!is_array($survey_fields)) {
          $this->buildFieldMapping($form, $survey_fields);
        }
        else {
          foreach ($survey_fields as $survey_field) {
            $this->buildFieldMapping($form, $survey_field);
          }
        }

      }
    }
    else {
      $form['convio_mapping']['description'] = [
        '#markup' => $this->t('Please select a survey in order to view mappings.'),
      ];
    }

    return $form;
  }

  /**
   * Helper function used to map webform elements to convio survey fields.
   *
   * @param array $form
   *   The form array.
   * @param object $survey_field
   *   The individual survey field returned from the api.
   */
  public function buildFieldMapping(array &$form, $survey_field) {
    // Create options array from list of existing webform elements.
    $webform_elements_options = ['' => $this->t('- Select -')];
    $webform_elements = $this->getWebform()->getElementsDecoded();
    foreach ($webform_elements as $key => $element) {
      $webform_elements_options[$key] = $element['#title'];
    }

    // If this is a ConsQuestion field then loop through contactInfoField
    // to find fields.
    if ($survey_field->questionType == 'ConsQuestion') {
      if (isset($survey_field->questionTypeData) &&
        isset($survey_field->questionTypeData->consRegInfoData) &&
        isset($survey_field->questionTypeData->consRegInfoData->contactInfoField)) {
        foreach ($survey_field->questionTypeData->consRegInfoData->contactInfoField as $field) {
          $form['convio_mapping'][$field->fieldName] = [
            '#type' => 'select',
            '#title' => $field->fieldName,
            // Note: $field->fieldStatus returns a string not a boolean.
            //'#required' => $field->fieldStatus == 'REQUIRED' ? TRUE : FALSE,
            '#options' => $webform_elements_options,
            '#default_value' => isset($this->configuration['convio_mapping'][$field->fieldName]) ? $this->configuration['convio_mapping'][$field->fieldName] : '',
          ];
        }
      }
    }
    else {
      $form['convio_mapping']['question_' . $survey_field->questionId] = [
        '#type' => 'select',
        '#title' => $survey_field->questionText,
        // Note: $survey_field->questionRequired returns a string not a boolean.
        //'#required' => $survey_field->questionRequired == 'true' ? TRUE : FALSE,
        '#options' => $webform_elements_options,
        '#default_value' => isset($this->configuration['convio_mapping']['question_' . $survey_field->questionId]) ? $this->configuration['convio_mapping']['question_' . $survey_field->questionId] : '',

      ];
    }
  }

  /**
   * Callback for the survey_id select field.
   */
  public function convioMappingCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['convio_mapping'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        $this->configuration[$name] = $values[$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {

    $submissions = $webform_submission->getData();

    $convio_params = [];
    if (isset($this->configuration['survey_id'])) {
      $convio_params['survey_id'] = $this->configuration['survey_id'];
    }
    else {
      $message = $this->t('Survey could not be submitted. Missing survey_id');
      drupal_set_message($message, 'error');
      $this->logger->error($message);
      return;
    }

    // Add additional survey params.
    if (is_array($this->configuration['convio_mapping']) && isset($this->configuration['convio_mapping'])) {
      $mapping = array_filter($this->configuration['convio_mapping']);
      foreach ($mapping as $key => $value) {
        $convio_params[$key] = $submissions[$value];
      }
    }
    else {
      $message = $this->t('Survey: @survey_id could not be submitted. Missing additional params.', ['@survey_id' => $this->configuration['survey_id']]);
      drupal_set_message($message, 'error');
      $this->logger->error($message);
      return;
    }

    /** @var \Drupal\convio_connect_webform\Plugin\ConvioEndpoint\SubmitSurvey $survey */
    $survey = $this->convioEndpointManager->createInstance('submitSurvey', $convio_params);
    $survey_result = $survey->postData();

    if (isset($survey_result->submitSurveyResponse) && isset($survey_result->submitSurveyResponse->success)) {
      $this->logger->info($this->t('Survey: @survey_id was submitted successfully.', ['@survey_id' => $this->configuration['survey_id']]));
    }
    else {
      if (isset($survey_result->errors->errorMessage)) {
        $message = Html::escape($survey_result->errors->errorMessage);
      }
      elseif (isset($survey_result->errorResponse->message)) {
        $message = Html::escape($survey_result->errorResponse->message);
      }
      else {
        $message = t('Convio Survey submission failed');
      }
      drupal_set_message($message, 'error');
      $this->logger->error($message);
    }
  }

}
